DROP DATABASE IF EXISTS `summer`;
CREATE DATABASE IF NOT EXISTS `summer` DEFAULT CHARACTER SET 'utf8';
USE `summer`;
-- DROP TABLE IF EXISTS `summer_article`;
-- /*!40101 SET @saved_cs_client     = @@character_set_client */;
-- /*!50503 SET character_set_client = utf8mb4 */;
-- CREATE TABLE `xzqa_article` (
--   `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章ID,主键且自增',
--   `subject` varchar(50) NOT NULL COMMENT '文章标题',
--   `description` varchar(255) NOT NULL COMMENT '文章简介',
--   `content` mediumtext NOT NULL COMMENT '文章正文',
--   `image` varchar(50) DEFAULT NULL COMMENT '文章缩略像',
--   `category_id` smallint(5) unsigned NOT NULL COMMENT '外键,文章分类ID',
--   `author_id` int(10) unsigned NOT NULL COMMENT '外键,作者ID',
--   `created_at` int(10) unsigned NOT NULL COMMENT '发表日期',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=1107 DEFAULT CHARSET=utf8;
-- /*!40101 SET character_set_client = @saved_cs_client */;
-- --
-- -- Dumping data for table `xzqa_article`
-- --
-- LOCK TABLES `xzqa_article` WRITE;
-- INSERT INTO `xzqa_article` (`id`, `subject`, `description`, `content`, `image`, `category_id`, `author_id`, `created_at`) VALUES
-- UNLOCK TABLES;

-- Table structure for table `xzqa_author`

DROP TABLE IF EXISTS `summer_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `summer_author` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID,主键且自增',
  `username` varchar(30) NOT NULL COMMENT '用户名,唯一',
  `phone` varchar(11) NOT NULL COMMENT '电话号码',
  `password` varchar(32) NOT NULL COMMENT '密码,MD5',
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(50) NOT NULL DEFAULT 'unnamed.jpg' COMMENT '用户头像',
  -- `article_number` mediumint(9) NOT NULL DEFAULT '0' COMMENT '发表的文章数量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE = InnoDB AUTO_INCREMENT = 16 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `summer_author`
--
LOCK TABLES `summer_author` WRITE;
/*!40000 ALTER TABLE `summer_author` DISABLE KEYS */;
INSERT INTO
  `summer_author` (
    `id`,
    `username`,
    `phone`,
    `password`,
    `nickname`,
    `avatar`
  )
VALUES
  (
    1,
    'Richard',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '黑色纯牛M奶',
    '00183a5ab206aea80120be1472a6f5.jpg'
  ),
  (
    2,
    'Johnny',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '风之谷z',
    '001f075ad3feeda8012138670b58f0.jpg'
  ),
  (
    3,
    'Martin',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '庚方丽理',
    '008c8f59e96a55a801216a4bbcbcb0.jpg'
  ),
  (
    4,
    'Christina',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '阐炜辉',
    '00adca5a0d93daa80121985c9ef05f.jpg'
  ),
  (
    5,
    'Margaret',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '伯启根',
    '00b2e259575a7da8012193a331099a.jpg'
  ),
  (
    6,
    'Barbara',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '旅行泡沫',
    '00b3755b2b6eb6a8012034f78d8b5b.jpg'
  ),
  (
    7,
    'Diana',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '燕雨y传',
    '00c1d55af1178ca801206abad941b6.jpg'
  ),
  (
    8,
    'Melody',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '稀稀哩哩',
    '00d1345abc83d5a801218207516561.jpg'
  ),
  (
    9,
    'Debbie',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '最爱Kitty',
    '00d4325a72b3b6a8012134661d177d.jpg'
  ),
  (
    10,
    'Scott',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '游客学者麦',
    '00da335a266f21a80120ba3858f56a.jpg'
  ),
  (
    11,
    'Shelly',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '天街小雨',
    '00fb2f5b2c9a39a8012034f76e8c48.jpg'
  ),
  (
    12,
    'Bob',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '浮云不说话',
    '010b6f5bb09ad5a8012099c8b8a41f.jpg'
  ),
  (
    13,
    'Amanda',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '左耳似水正',
    '011b395cdd9efaa801208f8b1fb812.jpg'
  ),
  (
    14,
    'George',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '吃草莓要吐籽',
    '011c4e5ba0fabba801213deacb693c.jpg'
  ),
  (
    15,
    'Benjamin',
    '13212345678',
    'e10adc3949ba59abbe56e057f20f883e',
    '没表有时间',
    '0120cd5dc0e038a801209e1fc96ef7.jpg'
  );
  /*!40000 ALTER TABLE `summer_author` ENABLE KEYS */;
UNLOCK TABLES;
--
  -- Table structure for table `summer_category`
  --
  DROP TABLE IF EXISTS `summer_category`;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `summer_category` (
    `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '类型ID,主键且自增',
    `category_name` varchar(30) NOT NULL COMMENT '类型名称,唯一',
    PRIMARY KEY (`id`),
    UNIQUE KEY `category_name` (`category_name`)
  ) ENGINE = InnoDB AUTO_INCREMENT = 5 DEFAULT CHARSET = utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;
--
  -- Dumping data for table `summer_category`
  --
  LOCK TABLES `summer_category` WRITE;
  /*!40000 ALTER TABLE `summer_category` DISABLE KEYS */;
INSERT INTO
  `summer_category` (`id`, `category_name`)
VALUES
  (1, 'UI'),(4, '交互'),(2, '电商'),(3, '网页');
  /*!40000 ALTER TABLE `summer_category` ENABLE KEYS */;
UNLOCK TABLES;