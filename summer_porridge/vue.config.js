module.exports={
  chainWebpack:config=>{
    config.plugins.delete("prefetch")
  //   //删除index.html开头的带有prefetch属性的link，不要异步下载暂时不需要的页面组件文件
  },


  devServer: {
    proxy: {
    //  本地服务器 
        // 用户名登录请求
       '/login1': {
        //要访问的跨域的域名
        target: `http://localhost:3000`,
        ws: true, // 是否启用websockets
        secure:false, // 使用的是http协议则设置为false，https协议则设置为true
        //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
        changeOrigin: true,
      }, 
        //  手机号登录请求  
      '/login2': {
        //要访问的跨域的域名
        target: `http://localhost:3000`,
        ws: true, // 是否启用websockets
        secure:false, // 使用的是http协议则设置为false，https协议则设置为true
        //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
        changeOrigin: true,
      },
         //   注册请求
      '/register1': {
        //要访问的跨域的域名
        target: `http://localhost:3000`,
        ws: true, // 是否启用websockets
        secure:false, // 使用的是http协议则设置为false，https协议则设置为true
        //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
        changeOrigin: true,
      },

    //   新的菜谱接口
      '/jisuapi': {
        //要访问的跨域的域名
        target: `https://way.jd.com`,
        ws: true, // 是否启用websockets
        secure:true, // 使用的是http协议则设置为false，https协议则设置为true
        //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
        changeOrigin: true,
      },
    //   旧的菜谱接口
    '/recipe': {
      //要访问的跨域的域名
      target: `https://api.binstd.com`,
      ws: true, // 是否启用websockets
      secure:true, // 使用的是http协议则设置为false，https协议则设置为true
      //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
      changeOrigin: true,
    },
      
    //   新闻接口
      '/jisuapi': {
        //要访问的跨域的域名
        target: `https://way.jd.com`,
        ws: true, // 是否启用websockets
        secure:true, // 使用的是http协议则设置为false，https协议则设置为true
        //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端端和服务端进行数据的交互就不会有跨域问题
        changeOrigin: true,
      },
    }
  },


}
