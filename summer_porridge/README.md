### 项目:`夏日粥品`   `移动端`
![图片](https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fdingyue.ws.126.net%2F2020%2F0501%2F590f6910j00q9m63g0017d200hs00csg00hs00cs.jpg&refer=http%3A%2F%2Fdingyue.ws.126.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1629033528&t=f4548bc347873381faec19679bd4c0fe)

#### 项目启动时间:2021/7/9

GIT地址  及时上传,及时更新
>`https://gitee.com/wu-changfu/summer.git`

>把所有的文件添加到暂存区
git add .
git commit -m "提交说明信息"

>查看当前所有分支
git branch

>切换分支
git checkout 分支名称

>合并分支
git merge 分支名称

>切换到主分支master，才能上传
拉取远程代码到本地
git pull https://gitee.com/wu-changfu/summer.git

>`拉取后本地代码和远程会同时存在,!!!!一定要合并后才能上传,!!!!千万不要 -f 上传git`
git push -u https://gitee.com/wu-changfu/summer.git master

项目环境设置命令

`npm install`

项目启动命令

`npm run serve`

##### 技术栈
`div`+`css`+`JavaScript`+`vue`+`Vant-UI`+`axios`+`vue-axios`+`Moment.js`

##### 项目描述
>夏日粥品是一款美食类的app,主打是粥类产品,里面有粥品的介绍、做法、养生食材、兼购买于一体的美食app

##### 主要模块
>吴昌富:(组长) 个人信息模块 `Me.vue`   购物车模块 `MyCart.vue`
张朝辉:  首页模块 `Home.vue`
吴 军:   分类模块 `Sort.vue`   
黄 业:   详情页页面 `Detail.vue`     搜索模块 `Search.vue`
陈垠泓:  菜谱模块 `Menu.vue` 
杨 浩:   登录模块 `Login.vue`  注册模块 `Register.vue`  数据库    接口编写
>###### 工作内容:
> + 1.页面:登录
>    + 用`Tab 标签页`组件实现切换`用户名`和`手机号登录`
>    + 标签绑定事件,切换时执行事件函数,清空表单里输入内容
>       + 1.`用户名登录功能`
>           + 表单用户名,密码
>           + 验证用户名,密码正则表达式(用户名:6-15位数字,字母大小写,密码:6位数字)
>           + 验证错误时,加入Toast弹窗提示
>           + 验证成功后,发送post请求,传参数用户名,密码到数据库
>           + 数据库收到数据查询数据,比对用户名和密码
>           + 把需要的信息从数据库请求回来,存到vuex里面,待定
>           + 登录成功后跳到首页,修改登录状态
>       + 2.`手机号登录功能`
>           + 表单手机号,密码
>           + 验证手机号,密码正则表达式(手机号:11位有效电话,密码:6位数字)
>           + 验证错误时,加入Toast弹窗提示
>           + 验证成功,发送post请求,传参数手机号,密码到数据库
>           + 数据库收到数据查询数据,比对手机号和密码
>           + 把需要的信息从数据库请求回来,存到vuex里面,待定
>           + 登录成功后跳到首页,修改登录状态
>+ 2.页面:注册
>    + 用vant组件搭建表单,用户名,昵称,手机号,短信验证码,密码,确认密码,地址
>    + 用户名输入框正则表达式验证(用户名:6-15位数字,字母大小写)
>    + 手机号输入框正则表达式验证(手机号:11位有效电话)
>    + 短信验证码功能,在输入手机号码后,手机号格式正确,按钮点击后才能执行
>    + 验证码随机生成,本地储存一份,用户收到一份,然后进行对比验证
>    + 防止用户反复请求验证码,在按钮点击后实现60s倒计时,并禁用按钮
>    + 密码输入框正则表达式验证(密码:6位数字)
>    + 确认密码正则表达式验证(密码:6位数字),并且验证和密码框输入是否一致
>    + 验证各个数据的正则表达式,所有数据填写完后,才可以点击提交发送请求
>    + 发送post请求,在数据库插入用户信息,以备登录使用
>+ 3.数据库
>    + 新建数据库:summer,用户表:summer_author
>    + 属性:用户名,手机号,密码,昵称,用户头像
>    + 两个登录请求(用户名,密码登录;手机号,密码登录),查询数据
>    + 一个注册请求(表单验证),插入数据
>    + 数据库只负责登录注册的请求,其它业务逻辑使用的网络接口

>+ 4.网络接口
>    + 1.新闻
>    + 2.菜谱


#### `大家齐心协力完成项目`

>##### 遇到的问题:
> **7月12日**
git的使用,报错,无法正常上传

> **7月13日**
git仓库出问题重建,项目底层vue框架配置错误
页面布局的一些小问题

> **7月14日**
样式全局污染,影响到其它页面
git上传误操作，影响远程仓库

> **7月15日**
共有头部,底部如何封装成全局组件
页面布局问题
页面适配问题

> **7月16日**
接口测试,出现cors同源跨域问题,使用http跨域解决
如何设置子路由跳转

> **7月17日**
http代理如何获取地址不同的接口的数据问题

> **7月19日**
各个模块,业务流程无法连接到一起,缺乏整体性,一致性

> **7月20日**
内部讨论出现分歧

> **7月21日**
数据请求到了,无法渲染到页面


##### 接口文档
>#### 新闻模块
>+ **1.获取新闻**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/news/get?channel=头条&start=0&num=10&appkey=ed9a9210c6639bc0`

+ ##### 请求参数
| 参数名称 | 类型   | 必填   |  说明              |
| :---:   | :---:  | :---:  | :---:              |
| channel | string | 是     | 频道               |
| num     | int    | 否     | 数量 默认10,最大40 |
| start   | int    | 否     | 起始位置,默认0     |

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
channel	|string	|频道|
num	|int	|数量
title	|string	|标题
time	|string	|时间
src	|string	|来源
category|	string	|分类
pic|	string	|图片
content|	string	|内容
url	|string	|原文手机网址
weburl	|string	|原文PC网址

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": {
        "channel": "头条",
        "num": "10",
        "list": [        
            {
                "title": "中国开闸放水27天解救越南旱灾",
                "time": "2016-03-16 07:23",
                "src": "中国网",
                "category": "mil",
                "pic": "http://api.jisuapi.com/news/upload/20160316/105123_31442.jpg",
                "content": "<p class="\"art_t\"">　　原标题：防总：应越南请求 中方启动澜沧江水电站水量应急调度</p><p class="\"art_t\"">　　记者从国家防总获悉，应越南社会主义共和国请求，我方启动澜沧江梯级水电站水量应急调度，缓解湄公河流域严重旱情。3月15日8时，澜沧江景洪水电站下泄流量已加大至2190立方米每秒，标志着应越方请求，由我方实施的澜沧江梯级水电站水量应急调度正式启动。</p>",
                "url": "http://mil.sina.cn/zgjq/2016-03-16/detail-ifxqhmve9235380.d.html?vt=4&pos=108",
                "weburl": "http://mil.news.sina.com.cn/china/2016-03-16/doc-ifxqhmve9235380.shtml"
            }
        ]
    }
}
```

>+ **2.获取新闻频道**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/news/channel?appkey=ed9a9210c6639bc0`

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": [
        "头条",
        "新闻",
        "财经",
        "体育",
        "娱乐",
        "军事",
        "教育",
        "科技",
        "NBA",
        "股票",
        "星座",
        "女性",
        "健康",
        "育儿"
    ]
}
```
>+ **3.搜索新闻**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/news/search?keyword=姚明&appkey=ed9a9210c6639bc0`

+ ##### 请求参数
| 参数名称 | 类型   | 必填   |  说明              |
| :---:   | :---:  | :---:  | :---:              |
keyword	|string	|是	|关键词

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
keyword	|string	|关键词
num	|int|	数量
title	|string|	标题
time	|string|	时间
src	|string	|来源
category|	string|	分类
pic	|string|	图片
url	|string	|原文手机网址
weburl|	string|	原文PC网址
content|	string|	内容

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": {
        "keyword": "姚明",
        "num": "9",
        "list": [
            {
                "title": "姚明:篮球改革比足球基础好 像电视剧一样播比赛",
                "time": "2016-03-16 09:59:06",
                "src": "网易",
                "category": "",
                "pic": "http://api.jisuapi.com/news/upload/20160316/104634_55612.jpg",
                "url": "http://m.news.so.com/transcode?ofmt=html&src=srp&q=%E5%A7%9A%E6%98%8E&pn=1&pos=1&m=20bf33d00f8db460ecacb72229acbd11f3d238e1&u=http%3A%2F%2Fsports.163.com%2F16%2F0316%2F09%2FBI96O41V00052UUC.html",
                "weburl": "http://sports.163.com/16/0316/09/BI96O41V00052UUC.html",
                "content": "<p>2 米26的姚明，任何时候都无比显眼。不过亮相“两会”的他，靠的不是身高，而是提案的“含金量”。作为全国政协委员，他今年关注的是体育场馆改革，建议可为体育馆发放年检的安全许可证，在许可证范围之内，举办赛事可以不用再经过公安、消防等部门的审批，从而真正盘活体育场馆。</p><p>不过，姚明的“真知灼见”可不止会上说的这些。在人民日报和人民网的“两会e客厅”，他敞开话匣子，说出了体育改革的不少“干货”。</p><p class="\"header\"">篮球改革比足球改革的基础还好</p><p>姚明:足球改革已经迈出了很坚实的一步，实现管办分离，这是中国足协实体化的一个初步结果。在这个前提之下，篮球改革也应该迎头赶上，迈出这样的脚步跟上去。</p><p>当然可能王婆卖瓜，我是篮球出身，我总感觉篮球改革的基础甚至比足球更好一些。有数据显示，中国有3亿的篮球爱好者，有62万块室内或室外的篮球场，这是一个非常雄厚的基础。同时，我们还有比较好的国家队成绩，是支撑这个体系的“塔尖”。某种程度上，篮球完全有理由跟上甚至迈出更坚实、更远的步伐。所以非常希望可以看到篮球改革也尽快启动。</p>"
            }
        ]
    }
}
```

>#### 菜谱大全模块
>+ **1.菜谱搜索**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/recipe/search?keyword=白菜&num=10&appkey=ed9a9210c6639bc0`

+ ##### 请求参数
| 参数名称 | 类型   | 必填   |  说明              |
| :---:   | :---:  | :---:  | :---:              |
keyword	|string|	是	|关键词
num	|int	|是	|获取数量

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
num	|int|	菜谱数量
id	|int|	菜谱ID
classid|	int|	分类ID
name	|string|	菜谱名称
peoplenum|	string|	用餐人数
preparetime	|string|	烹饪时间
cookingtime	|string|	烹饪时间
content	|string|	菜谱说明
pic	|string|	菜谱图片
tag	|string|	标签
material	|string|	材料
mname	|string|	材料名称
type	|int|	材料类型 0辅料 1主料
amount	|string|	数量
process	|string|	步骤
pcontent	|string|	步骤内容

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": {
        "num": "10",
        "list": [
            {
                "id": "8",
                "classid": "2",
                "name": "醋溜白菜",
                "peoplenum": "1-2人",
                "preparetime": "10-20分钟",
                "cookingtime": "10-20分钟",
                "content": "醋溜白菜，是北方人经常吃的一道菜，尤其是在多年前的冬天。那时，没有大棚菜，冬天，家家每天佐餐的基本上都是冬储大白菜，聪明的家庭主妇总是想方设法将这单调的菜变成多种菜式，于是，醋溜白菜被频繁的端上餐桌。<br>美食不分贵贱，用最平凡的原料、最简单的调料和最普通的手法做出美味的菜肴来才是美食的真谛。 <br>这次，我做的醋溜白菜，近似鲁菜的做法，使个这道菜酸甜浓郁、开胃下饭、老少咸宜。",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/115138_46688.jpg",
                "tag": "减肥,家常菜,排毒,补钙",
                "material": [
                    {
                        "mname": "油",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "盐",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "花椒",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "干红椒",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "葱",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "姜",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "蒜",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "醋",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "酱油",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "糖",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "淀粉",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "白菜",
                        "type": "1",
                        "amount": "380g"
                    }
                ],
                "process": [
                    {
                        "pcontent": "准备食材。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162550_84583.jpg"
                    },
                    {
                        "pcontent": "将白菜斜刀片成薄片。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162551_90620.jpg"
                    },
                    {
                        "pcontent": "片切好的白菜帮与菜叶分别入好。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162551_20925.jpg"
                    },
                    {
                        "pcontent": "盐、糖、生抽、醋淀粉加少许水调匀备用。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162552_23125.jpg"
                    },
                    {
                        "pcontent": "锅中油烧热，先入花椒炒香后捞出。再加入干红椒段略炒。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162552_57046.jpg"
                    },
                    {
                        "pcontent": "加入葱姜蒜煸炒香，然后入白菜帮翻炒。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162553_89090.jpg"
                    },
                    {
                        "pcontent": "炒至菜帮变软时，加入白菜叶。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162553_40445.jpg"
                    },
                    {
                        "pcontent": "快速翻炒至菜软，勾入碗汁",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162554_92210.jpg"
                    },
                    {
                        "pcontent": "使汤汁均匀的包裹在菜帮上即可",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162554_29522.jpg"
                    }
                ]
            }
        ]
    }
}
```

>+ **2.菜谱分类**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` 
请求示例：
`https://api.binstd.com/recipe/class?appkey=ed9a9210c6639bc0`

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
classid	|int|	分类ID
name	|string|	菜谱名称
parentid	|int|	上级ID

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": [
        {
            "classid": "1",
            "name": "功效",
            "parentid": "0",
            "list": [
                {
                    "classid": "2",
                    "name": "减肥",
                    "parentid": "1"
                },
                {
                    "classid": "3",
                    "name": "瘦身",
                    "parentid": "1"
                },
                {
                    "classid": "4",
                    "name": "消脂",
                    "parentid": "1"
                },
                {
                    "classid": "5",
                    "name": "丰胸",
                    "parentid": "1"
                },
                {
                    "classid": "6",
                    "name": "美容",
                    "parentid": "1"
                },
                {
                    "classid": "7",
                    "name": "养颜",
                    "parentid": "1"
                },
                {
                    "classid": "8",
                    "name": "美白",
                    "parentid": "1"
                },
                {
                    "classid": "9",
                    "name": "防晒",
                    "parentid": "1"
                },
                {
                    "classid": "10",
                    "name": "排毒",
                    "parentid": "1"
                },
                {
                    "classid": "11",
                    "name": "祛痘",
                    "parentid": "1"
                },
                {
                    "classid": "12",
                    "name": "祛斑",
                    "parentid": "1"
                },
                {
                    "classid": "13",
                    "name": "保湿",
                    "parentid": "1"
                },
                {
                    "classid": "14",
                    "name": "补水",
                    "parentid": "1"
                },
                {
                    "classid": "15",
                    "name": "通乳",
                    "parentid": "1"
                },
                {
                    "classid": "16",
                    "name": "催乳",
                    "parentid": "1"
                },
                {
                    "classid": "17",
                    "name": "回奶",
                    "parentid": "1"
                },
                {
                    "classid": "18",
                    "name": "下奶",
                    "parentid": "1"
                },
                {
                    "classid": "19",
                    "name": "调经",
                    "parentid": "1"
                },
                {
                    "classid": "20",
                    "name": "安胎",
                    "parentid": "1"
                },
                {
                    "classid": "21",
                    "name": "抗衰老",
                    "parentid": "1"
                },
                {
                    "classid": "22",
                    "name": "抗氧化",
                    "parentid": "1"
                },
                {
                    "classid": "23",
                    "name": "延缓衰老",
                    "parentid": "1"
                },
                {
                    "classid": "24",
                    "name": "补钙",
                    "parentid": "1"
                },
                {
                    "classid": "25",
                    "name": "补铁",
                    "parentid": "1"
                },
                {
                    "classid": "26",
                    "name": "补锌",
                    "parentid": "1"
                },
                {
                    "classid": "27",
                    "name": "补碘",
                    "parentid": "1"
                },
                {
                    "classid": "28",
                    "name": "补硒",
                    "parentid": "1"
                }
            ]
        }
    ]
}
```

>+ **3.按分类检索**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/recipe/byclass?classid=2&start=0&num=10&appkey=ed9a9210c6639bc0`

+ ##### 请求参数
| 参数名称 | 类型   | 必填   |  说明              |
| :---:   | :---:  | :---:  | :---:              |
classid	|int|	是|	分类ID
start	|int|	是|	起始条数，默认0
num	|int	|是|	获取数量

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
num	|int|	菜谱数量
id	|int|	菜谱ID
classid|	int|	分类ID
name	|string|	菜谱名称
peoplenum|	string|	用餐人数
preparetime	|string|	烹饪时间
cookingtime	|string|	烹饪时间
content	|string|	菜谱说明
pic	|string|	菜谱图片
tag	|string|	标签
material	|string|	材料
mname	|string|	材料名称
type	|int|	材料类型 0辅料 1主料
amount	|string|	数量
process	|string|	步骤
pcontent	|string|	步骤内容

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": {
        "num": "10",
        "list": [
            {
                "id": "1",
                "classid": "2",
                "name": "炸茄盒",
                "peoplenum": "3-4人",
                "preparetime": "10分钟内",
                "cookingtime": "10-20分钟",
                "content": "炸茄盒外焦里嫩，入口不腻，咬上一口，淡淡的肉香和茄子的清香同时浮现，让人心动不已。",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/115137_60657.jpg",
                "tag": "健脾开胃,儿童,减肥,宴请,家常菜,小吃,炸,白领,私房菜,聚会",
                "material": [
                    {
                        "mname": "鸡蛋",
                        "type": "0",
                        "amount": "1个"
                    },
                    {
                        "mname": "姜",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "蒜子",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "葱",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "盐",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "白糖",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "料酒",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "酱油",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "老抽",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "胡椒粉",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "麻油",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "面粉",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "生粉",
                        "type": "0",
                        "amount": "适量"
                    },
                    {
                        "mname": "茄子",
                        "type": "1",
                        "amount": "适量"
                    },
                    {
                        "mname": "猪肉",
                        "type": "1",
                        "amount": "300g"
                    }
                ],
                "process": [
                    {
                        "pcontent": "首先我们将猪肉剁成肉泥、姜切成姜米、葱切葱花、蒜子切成蒜末、茄子去皮，然后在每一小段中间切一刀，但不要切断，做成茄盒。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162541_29953.jpg"
                    },
                    {
                        "pcontent": "然后我们来制作肉馅：将猪肉泥放入盘中，加入姜米、蒜末、葱花、少许盐、少许白糖、适量料酒、少量酱油、少量老抽、少许胡椒粉、淋入食用油、麻油，抓匀，接着再加入生粉，抓打至肉馅变得粘稠。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162541_42272.jpg"
                    },
                    {
                        "pcontent": "将茄夹中间抹上生粉，用肉馅填满茄夹。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162541_24315.jpg"
                    },
                    {
                        "pcontent": "填满肉馅后，再逐一将整个茄盒抹上生粉。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162542_72995.jpg"
                    },
                    {
                        "pcontent": "接着我们来调面糊：准备一个碗，在碗中放入适量面粉、适量生粉、半勺盐、一个鸡蛋拌匀，再加少许清水，拌至粘稠状，备用。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162542_12103.jpg"
                    },
                    {
                        "pcontent": "锅中放入半锅油，烧至8成热后，将茄盒放入面糊中裹上面糊，再逐个下油锅中，炸至茄盒表面呈金黄色后捞出沥油。这道菜就完成了。",
                        "pic": "http://api.jisuapi.com/recipe/upload/20160719/162542_45617.jpg"
                    }
                ]
            }
        ]
    }
}
```

>+ **4.根据ID查询详情**
接口地址：
`https://api.binstd.com`
请求方法：
`GET` `POST`
请求示例：
`https://api.binstd.com/recipe/detail?id=5&appkey=ed9a9210c6639bc0`

+ ##### 请求参数
| 参数名称 | 类型   | 必填   |  说明              |
| :---:   | :---:  | :---:  | :---:              |
id	|int|	是|	菜谱ID

+ ##### 返回参数
| 参数名称 | 类型 | 说明 |
| :---: | :---: |:---:  |
id	|int|	菜谱ID
classid|	int|	分类ID
name	|string|	菜谱名称
peoplenum|	string|	用餐人数
preparetime	|string|	烹饪时间
cookingtime	|string|	烹饪时间
content	|string|	菜谱说明
pic	|string|	菜谱图片
tag	|string|	标签
material	|string|	材料
mname	|string|	材料名称
type	|int|	材料类型 0辅料 1主料
amount	|string|	数量
process	|string|	步骤
pcontent	|string|	步骤内容

+ ##### JSON返回示例 :
```
{
    "status": "0",
    "msg": "ok",
    "result": {
        "id": "5",
        "classid": "2",
        "name": "翡翠彩蔬卷",
        "peoplenum": "1-2人",
        "preparetime": "10分钟内",
        "cookingtime": "10分钟内",
        "content": "春天是为夏天做准备的刮油季，为了夏天能够美美的穿上漂亮的花裙子，让我们一起来狠狠的刮油吧。<br>这个色彩缤纷的彩蔬卷，低热量，高营养，是一道秀色可餐的减肥餐~",
        "pic": "http://api.jisuapi.com/recipe/upload/20160719/115138_19423.jpg",
        "tag": "减肥,咸香,宴请,抗氧化,抗衰老,私房菜,聚会",
        "material": [
            {
                "mname": "盐",
                "type": "0",
                "amount": "1勺"
            },
            {
                "mname": "鲍汁",
                "type": "0",
                "amount": "1茶勺"
            },
            {
                "mname": "糖",
                "type": "0",
                "amount": "适量"
            },
            {
                "mname": "水淀粉",
                "type": "0",
                "amount": "1勺"
            },
            {
                "mname": "大白菜",
                "type": "1",
                "amount": "3片"
            },
            {
                "mname": "菠菜",
                "type": "1",
                "amount": "30g"
            },
            {
                "mname": "红萝卜",
                "type": "1",
                "amount": "50g"
            },
            {
                "mname": "彩椒",
                "type": "1",
                "amount": "50g"
            }
        ],
        "process": [
            {
                "pcontent": "彩椒，红萝卜切丝",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_72503.jpg"
            },
            {
                "pcontent": "白菜和菠菜飞水",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_29860.jpg"
            },
            {
                "pcontent": "将蔬菜丝码在白菜叶上",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_92740.jpg"
            },
            {
                "pcontent": "卷成卷",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_38394.jpg"
            },
            {
                "pcontent": "切成段",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_26158.jpg"
            },
            {
                "pcontent": "锅中放少许水，加入盐，鲍汁（蚝油），水淀粉，煮至汤汁粘稠",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_40907.jpg"
            },
            {
                "pcontent": "浇在彩蔬卷上即可",
                "pic": "http://api.jisuapi.com/recipe/upload/20160719/162546_40860.jpg"
            }
        ]
    }
}
```

## `END`

