import axios from "axios";
export const getSearch = (val) => {
  return axios.get(
    `/recipe/search?keyword=${val}&num=20&appkey=5a432523cf279ce2`
  );
  // .then((res) => {
  //   return res.data.result.list;
  // });
};
// this.axios
//       .get("/recipe/search?keyword=稀饭&num=40&appkey=5a432523cf279ce2")
//       .then((res) => {
//         this.arrs = res.data.result.list;
//       });
// export const
export default { getSearch };
