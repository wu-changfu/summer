import axios from "axios";
export const getCpFlSearch = (val) => {
  return axios.get(
    `/jisuapi/search?keyword=${val}&appkey=b0e7418fe9bef46dcb47a53374ffbef1`
  );

  //   .then((result) => {
  //     this.simg = result.data.result.result.list;
};
export const getCpFlClassid = (classid) => {
  return axios.get(
    `/jisuapi/byclass?classid=${classid}&start=20&num=40&appkey=13e76141171ebe61f22e24477b559988`
  );
  // (async (ral) => {
  //   const res = await axios.get(`/jisuapi/search?keyword=${val}&appkey=b0e7418fe9bef46dcb47a53374ffbef1`);
  //   this.foodslist = res.data.result.result.list;
  // })();
};
export default { getCpFlSearch, getCpFlClassid };
