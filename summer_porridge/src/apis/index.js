// vue项目中 使用require.context()实现前端工程化引入文件
/*require.context(directory, useSubdirectories, regExp, mode);
-directory:表示检索的目录
-useSubdirectories：表示是否检索子文件夹
-regExp:匹配文件的正则表达式,一般是文件名
-mode:加载模式，同步/异步
-keys() 是一个函数,它返回上下文模块可以处理的所有可能请求的数组*/
const requireModule = require.context("./modules", false, /\.js/);
const modules = {};

requireModule.keys().forEach((item) => {
  const moduleName = item.substring(2, item.length - 3);
  modules[moduleName] = requireModule(item).default;
});

export default {
  install(Vue) {
    // 公共请求方法
    // console.log({ ...modules });
    Vue.prototype.$api = { ...modules };
  },
};
