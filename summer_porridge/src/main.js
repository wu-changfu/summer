import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import apis from "./apis";

// Vant组件库
import Vant from "vant";
import "vant/lib/index.css";
Vue.use(Vant);

// axios配置   第三方组件
import axios from "axios";
import VueAxios from "vue-axios";
// axios.defaults.baseURL = "http://localhost:3000";
axios.defaults.baseURL = "/";
Vue.use(VueAxios, axios);
Vue.use(apis);

//模拟后台数据交互
// import VueResource from 'vue-resource'
// Vue.use( VueResource )

//无限滚动
import infiniteScroll from "vue-infinite-scroll";
Vue.use(infiniteScroll);

//moment时间配置
import moment from "moment";
moment.locale("zh-cn"); //设置语言 或 moment.lang('zh-cn');
Vue.prototype.$moment = moment;

//动画图
import animated from "animate.css";
Vue.use(animated);

//封装头部搜索样式 全局组件
// import Header_bar from "./components/Header_bar";
// Vue.component("header-bar", Header_bar);

//封装底部导航 全局组件
import Tabbar from "./components/Tabbar";
Vue.component("xrzp-tabbar", Tabbar);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
