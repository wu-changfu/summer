import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";

import lianxi from "@/views/lian_xi.vue";
Vue.use(VueRouter);

const routes = [
  { path: "/lx", component: lianxi },
  
  //登录
  {
    path: "/login",
    name: "Login",
    component: () => import(/* webpackChunkName: "Login" */ "../views/Login"),
  },
  //注册
  {
    path: "/register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "Register" */ "../views/Register"),
  },
  // me
  {
    path: "/me",
    name: "Me",
    component: () => import(/* webpackChunkName: "Me" */ "../views/Me"),
  },
  // 新闻
  {
    path: "/journalism",
    name: "Journalism",
    component: () =>
      import(
        /* webpackChunkName: "Journalism" */ "../views/Journalism/index.vue"
      ),
  },
  // 新闻详情
  {
    path: "/article/:id",
    name: "Article",
    component: () =>
      import(/* webpackChunkName: "Article" */ "../views/Journalism/Article"),
  },
  // 分类详情
  {
    path: "/xiangqing/:id",
    component: () =>
      import(/* webpackChunkName: "xiangqing" */ "../views/Fenlei/Xiangqing"),
    meta: {
      //配置是否需要
      isYunxu: true,
      title: "收藏夹",
    },
  },
  // 分类
  {
    path: "/fenlei",
    name: "Fenlei",
    component: () =>
      import(/* webpackChunkName: "Fenlei" */ "../views/Fenlei/index.vue"),
  },
  // home吃货...
  {
    path: "/together",
    name: "Together",
    component: () =>
      import(/* webpackChunkName: "Together" */ "../views/Home/Together"),
  },
  // home 吃货 >常见问题
  {
    path: "/questions",
    name: "Questions",
    component: () =>
      import(/* webpackChunkName: "Questions" */ "@/views/Home/Questions"),
  },
  // 菜谱模块
  {
    path: "/caipu",
    name: "Caipu",
    component: () =>
      import(/* webpackChunkName: "Caipu" */ "../views/Caipu/index.vue"),
  },
  //  菜谱
  {
    path: "/caipuDetails/:id",
    component: () =>
      import(
        /* webpackChunkName: "caipuDetails" */ "../views/Caipu/CaipuDetails"
      ),
    meta: { title: "菜谱详情页" },
  },
  // 个人信息 wode
  {
    path: "/wode",
    name: "Wode",
    component: () =>
      import(/* webpackChunkName: "Wode" */ "../views/Wode/index.vue"),
    // beforeEnter: (to, from, next) => {
    //   // ...
    // }
    // children: [
    //   {
    //     path: "",
    //     name: "",
    //     component: i,
    //   },
    // ],
  },
  // 购物车
  {
    path: "/gouwuche",
    component: () =>
      import(/* webpackChunkName: "Gouwuche" */ "../views/Gouwuche"),
    meta: {
      //配置是否需要
      isGwc: true,
      title: "购物车",
    },
  },
  // 收藏
  {
    path: "/shoucang",
    component: () =>
      import(/* webpackChunkName: "shoucang" */ "../views/Wode/Shoucang"),
    meta: {
      //配置是否需要
      isSc: true,
      title: "收藏夹",
    },
    beforeEnter: (to, from, next) => {
      if (to.meta.isSc && !store.state.islogin) {
        // console.log(store);
        next({ path: "/login" });
      } else {
        next();
      }
    },
  },
  // 足迹
  {
    path: "/zuji",
    component: () =>
      import(/* webpackChunkName: "Zuji" */ "../views/Wode/Zuji"),
  },
  // 消息
  {
    path: "/xiaoxi",
    component: () =>
      import(/* webpackChunkName: "Xiaoxi" */ "../views/Wode/Xiaoxi"),
  },
  // 练习客服
  {
    path: "/lianxikefu",
    component: () =>
      import(/* webpackChunkName: "Lianxikefu" */ "../views/Wode/Lianxikefu"),
  },
  // 设置
  {
    path: "/shezhi",
    component: () =>
      import(/* webpackChunkName: "Shezhi" */ "../views/Wode/Shezhi"),
  },
  //首页
  {
    path: "/homes",
    name: "Homes",
    component: () =>
      import(/* webpackChunkName: "Homes" */ "../views/Home/index.vue"),
  },
  // 广告
  {
    path: "/",
    name: "Guanggao",
    component: () =>
      import(/* webpackChunkName: "Guanggao" */ "../views/Guanggao"),
  },
  // home 详情
  {
    path: "/articles",
    component: () =>
      import(/* webpackChunkName: "articles" */ "../views/Home/Ariticles"),
  },

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// // 全局前置守卫   主要 用于登录验证
//  /**
//      * @param {to} 将要去的路由
//      * @param {from} 出发的路由
//      * @param {next} 执行下一步
//      */
router.beforeEach((to, from, next) => {
  // document.title = to.meta.title || '卖座电影';
  //判断当前路由是否需要进行权限控制
  //    分类 详情页的设置 -->  到购物车模块
  if (to.meta.isGwc && !store.state.islogin) {
    // console.log(store);
    // console.log();
    next({ path: "/login" });
  } else {
    next();
  }
});
export default router;
