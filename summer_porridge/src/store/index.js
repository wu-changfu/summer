import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    islogin: false, //保存是否已登录成功
    username: "", //用户名
    nickname: "", //昵称
    avatar: {}, //头像
    phone: "", //电话号码
    // gogwc:true,   //加入购物车
    jsshopping: [], //加入购物车数据保存他
    scj: [], //收藏夹
    new: [],
  },
  mutations: {
    //this.$store.commit('loginOK','xx')自动执行
    // 登录后保存用户登录信息
    loninOK(state, newname) {
      state.islogin = newname[0] ? true : false;
      state.username = newname[0];
      state.nickname = newname[1];
      state.avatar = newname[2]
        ? require(`../assets/avatar/${newname[2]}`)
        : "";
      state.phone = newname[3];
    },
    //判断是否加入购物车
    //this.$store.commit('gwc','')
    gwc(state, newname) {
      // state.gogwc=false;
      state.jsshopping.push(newname);
    },
    //加入收藏夹
    jscj(state, newname) {
      state.scj.push(newname);
    },
    // 新闻详情
    news(state, newname) {
      state.new = newname;
    },
  },
  plugins: [
    createPersistedState({
      storage: window.sessionStorage,
    }),
  ],
});
